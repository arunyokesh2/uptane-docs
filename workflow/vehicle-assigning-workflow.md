## Uptane - vehicle assigning workflow - version 1

```mermaid
flowchart TD
    A[deployment with vehicle and image details] --> B[vehicles in the deployment list will be updated with the new image metadata details]
    B --> C[verify the metadata]
    C --> D[Update the metadata to S3 bucket for backup]
    D --> E[Update the target, snapshot, timestamp role files details to the job document]
    E --> F[update the deployment status]
```

## Uptane - vehicle assigning workflow - version 2

```mermaid
flowchart TD
    A[deployment with vehicle and image details] --> B[download metadata from S3 for all vehicles in the list]
    B --> C[check vehicle exist in local or stop the process]
    B --> D[vehicles in the deployment list will be updated with the new image metadata details and add it in dlog]
    D --> E[initiate verification process]
    E --> F[verify the metadata based on dlog]
    F --> G[Update the metadata to S3 bucket for backup]
```
## Publish deployment

```mermaid
flowchart TD
    A[publish deployment] --> B[get metadata from S3 bucket]
    B --> C[Update the target, snapshot, timestamp role files details to the job document]
    C --> D[update the deployment status to published]
```
