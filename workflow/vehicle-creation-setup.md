## Uptane - vehicle creation setup - version 1

```mermaid
flowchart TD
    A[create a new vehicle] --> B[IOT core queue]
    B --> C[listen to the Queue and initiate vehicle folder and metadata creation]
    C --> D[reuse the root file backup, if there is any]
    D --> E[reuse and check the keyid mapping]
    E --> F[verify the metadata files]
    F --> G[update the metadata files in the S3 backup]
```

## Uptane - vehicle creation setup - version 2

```mermaid
flowchart TD
    A[create a new vehicle] --> B[IOT core queue]
    B --> C[listen to the Queue and initiate vehicle folder and metadata creation]
    C --> D[reuse the root file backup if there is any]
    D --> E[reuse and check the keyid mapping]
    E --> F[add the vehicle details to dlog]
    F --> G[initiate the verification process]
    G --> H[verify metdata based on dlog]
    H --> I[push it to S3 bucket for backup]
    I --> J[clean up dlog local storage]
```


## Uptane - vehicle creation setup - version 3

```mermaid
flowchart TD
    A[create a new vehicle] --> B[IOT core queue]
    B --> C[listen to the Queue and initiate vehicle folder and metadata creation]
    C --> D[reuse the root file backup if there is any]
    D --> E[reuse and check the keyid mapping]
    E --> F[initiate the verification process]
    F --> G[push it to S3 bucket for backup]
    G --> H[clean up metadata and local storage]
```
