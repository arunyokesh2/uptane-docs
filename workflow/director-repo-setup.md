## Uptane - director repo setup workflow

```mermaid
flowchart TD
    A[Create base and director repo folders] --> B[Create symmentric keys for director repo roles]
    B --> C[Setup S3 bucket to upload director repo metadata files for backup]
    C --> D[Store root file in the vehicle local storage]
    D --> E[Initiate keyid mapping file]
    E --> F[Map keyid and public for director repo and reuse it]
    F --> G[Reuse the root file for new vehicle creation]
```
