## Uptane - image upload workflow

```mermaid
flowchart TD
    A[Get STS token from backend] --> B[Image upload status: INIT]
    B --> C[Upload a new image with STS Token]
    C --> D[Once the uploading process is done, update the status: PROCESSING]
    D --> E[Lambda will trigger the api with uploaded image details]
    E --> F[Server will get the image details and update the image checksum details to target files]
    F --> G[Verify the metadata files]
    G --> H[Update the image status: VERIFIED]
    H --> I[Update the metadata files in the S3 bucket]
```
