## Uptane - deployment workflow

```mermaid
flowchart TD
    A[Job document will be updated with target, snapshot, timestamp details] --> B[It will be uploaded to S3]
    B --> C[available for vehicle download]
```
