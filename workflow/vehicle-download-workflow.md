## Uptane - vehicle download workflow

```mermaid
flowchart TD
    A[Vehicle comes online] --> B[It will start download the job document]
    B --> C[Get correct time from the timeserver]
    C --> D[Verify the timeserver time with job document timestamp, snapshot and target meta details]
    D --> E[Check the role files stored in the local, if not save it after verification]
    E --> F[Now download the metadata files from image repo and verify it]
    F --> G[It will be stored in the local storage]
    G --> H[Compare target file with image repo, job document and local storage, if there is any new updates available to download]
    H --> I[Download the new image]
    I --> J[Verify the checksum of downloaded image with image repo and job document]
    J --> K[Check ECU details and store it to the secondary unverified folder]
    K --> L[After secondary ECU verification, its ready to install]
```

### Sequence Diagram

```mermaid
sequenceDiagram
    OTA_Director_Repo-->>Primary_ECU: Download job document
    Primary_ECU-->>Timeserver: Request for time from timeserver
    Timeserver-->>Primary_ECU: Getting correct current time
    Primary_ECU-->>Primary_ECU: Verify the timeserver time with job document timestamp, snapshot and target meta details
    Primary_ECU-->>Local_storage: Check the role files stored in the local, if not save it after verification 
    OTA_Image_Repo-->>Primary_ECU: Now download the metadata files from image repo and verify it
    Primary_ECU-->>Local_storage: Image repo metadata will be stored in the local storage
    Primary_ECU-->>Primary_ECU: Check any new updates available to download
    OTA_Image_Repo-->>Primary_ECU: Download new image
    Primary_ECU-->>Primary_ECU: Verify the checksum of downloaded image with image repo and job document
    Primary_ECU-->>Secondary_ECU: Check ECU details and store it to the secondary unverified folder
```
