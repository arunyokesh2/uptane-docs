## Uptane - vehicle ECU setup

```mermaid
flowchart TD
    A[New vehicle setup] --> B[create local storage folders for image and director repo]
    B --> C[each repo will have current and past folder for metadata backup]
    C --> D[save the image repo and director repo root file in local storage current folder]
    D --> E[store the server details to access]
```
