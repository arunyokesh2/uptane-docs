## Uptane - image repo setup workflow

```mermaid
flowchart TD
    A[Create base and image repo folders] --> B[Create symmentric keys for image repo roles]
    B --> C[create metafiles for root, target, snapshot and timestamp]
    C --> D[setup S3 bucket to upload image repo metadata and target files]
    D --> E[create a lambda trigger for S3 upload]
    E --> F[Create a metadata generator API for Lambda trigger]
    F --> G[Store root file in the vehicle local storage]
```
